package com.beone.generator;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.builder.ConfigBuilder;
import com.baomidou.mybatisplus.generator.config.po.TableField;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;

import java.util.List;

/**
 * @title  基于Web页面 生成文件
 * @Author 覃球球
 * @Version 1.0 on 2018/11/1.
 * @Copyright 贝旺科技
 */
public class WebAutoGenerator extends AutoGenerator{

    private List<TableInfo> tableInfos;

    public void setTableInfos(List<TableInfo> tableInfos) {
        this.tableInfos = tableInfos;
    }

    /**
     * 初始化配置
     */
    public void initConfig() {
        super.initConfig();
        this.tableInfos = getAllTableInfoList();
    }


    public List<TableInfo> getAllTableInfoList(){
        return config.getTableInfoList();
    }

    /**
     * <p>
     * 开放表信息、预留子类重写
     * </p>
     *
     * @param config 配置信息
     * @return
     */
    public List<TableInfo> getAllTableInfoList(ConfigBuilder config) {
        return tableInfos;
    }
}
