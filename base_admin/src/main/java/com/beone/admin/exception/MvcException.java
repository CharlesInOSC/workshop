package com.beone.admin.exception;

/**
 * @title
 * @Author 覃球球
 * @Version 1.0 on 2018/2/1.
 * @Copyright 长笛龙吟
 */
public class MvcException extends Exception{

    public MvcException(String name){
        super(name);
    }
}
