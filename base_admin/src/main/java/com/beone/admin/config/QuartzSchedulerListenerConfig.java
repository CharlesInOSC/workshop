package com.beone.admin.config;

import com.beone.admin.quartz.MyJobFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

/**
 * @Title  QuartzSchedulerListenerConfig配置
 * @Author 覃球球
 * @Version 1.0 on 2018-10-25
 * @Copyright 贝旺科权
 */
@Configuration
public class QuartzSchedulerListenerConfig {
      
    @Autowired
    MyJobFactory myJobFactory;

      
    @Bean(name ="schedulerFactoryBean")
    public SchedulerFactoryBean schedulerFactory() {
        SchedulerFactoryBean bean = new SchedulerFactoryBean();
        bean.setJobFactory(myJobFactory);  
        return bean;  
    }  
  
}  